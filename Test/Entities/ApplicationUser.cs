﻿using Microsoft.AspNetCore.Identity;

namespace Test.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public string DisplayName { get; set; }
    }
}
