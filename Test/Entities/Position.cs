﻿namespace Test.Entities
{
    public class Position
    {
        public ulong ID { get; set; }
        public string Name { get; set; }
    }
}
