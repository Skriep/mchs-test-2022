﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Web_053502_Sachivko.Utils.ValidationAttributes;

namespace Test.Entities
{
    public class Staff
    {
        public ulong ID { get; set; }

        [DisplayName("Имя")]
        public string Name { get; set; }

        [DisplayName("Фамилия")]
        public string Surname { get; set; }

        [DisplayName("Отчество")]
        public string Patronymic { get; set; }

        [DisplayName("Год рождения")]
        [YearOfBirth]
        public int BirthYear { get; set; }

        [DisplayName("Дата трудоустройства")]
        public DateTime AssignmentDate { get; set; }

        [DisplayName("Должность")]
        public Position? Position { get; set; }
        public ulong PositionID { get; set; }
    }
}
