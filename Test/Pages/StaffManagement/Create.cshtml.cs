﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Test.Data;
using Test.Entities;

namespace Test.Pages.StaffManagement
{
    public class CreateModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public SelectList Positions { get; private set; }

        public CreateModel(Test.Data.ApplicationDbContext context)
        {
            _context = context;
            Positions = new SelectList(_context.Positions, nameof(Position.ID), nameof(Position.Name));
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Staff Staff { get; set; }
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
          if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Staff.Add(Staff);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
