﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Test.Data;
using Test.Entities;

namespace Test.Pages.StaffManagement
{
    public class DeleteModel : PageModel
    {
        private readonly Test.Data.ApplicationDbContext _context;

        public DeleteModel(Test.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
      public Staff Staff { get; set; }

        public async Task<IActionResult> OnGetAsync(ulong? id)
        {
            if (id == null || _context.Staff == null)
            {
                return NotFound();
            }

            var staff = await _context.Staff
                                        .Include(c => c.Position)
                                        .FirstOrDefaultAsync(m => m.ID == id);

            if (staff == null)
            {
                return NotFound();
            }
            else 
            {
                Staff = staff;
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(ulong? id)
        {
            if (id == null || _context.Staff == null)
            {
                return NotFound();
            }
            var staff = await _context.Staff.FindAsync(id);

            if (staff != null)
            {
                Staff = staff;
                _context.Staff.Remove(Staff);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
