﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Test.Data;
using Test.Entities;

namespace Test.Pages.StaffManagement
{
    public class IndexModel : PageModel
    {
        private readonly Test.Data.ApplicationDbContext _context;

        public IndexModel(Test.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Staff> Staff { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Staff != null)
            {
                Staff = await _context.Staff
                                        .Include(c => c.Position)
                                        .ToListAsync();
            }
        }
    }
}
