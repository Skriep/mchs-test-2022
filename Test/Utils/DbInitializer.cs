﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Test.Data;
using Test.Entities;

namespace Test.Utils
{
    public static class DbInitializer
    {
        public static async Task Initialize(WebApplication app)
        {
            IServiceProvider serviceProvider = app.Services.CreateScope().ServiceProvider;
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var dbContext = serviceProvider.GetRequiredService<ApplicationDbContext>();

            await dbContext.Database.MigrateAsync();

            await InitUsers(userManager, roleManager);
            await InitStaff(dbContext);

            await dbContext.SaveChangesAsync();
        }

        private static async Task InitUsers(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            if (!await roleManager.RoleExistsAsync("admin"))
            {
                await roleManager.CreateAsync(new IdentityRole("admin"));
            }

            List<(ApplicationUser User, string Password, IEnumerable<string> Roles)> testAccounts = new()
            {
                (
                    new ApplicationUser(){
                        UserName = "administrator@example.com",
                        Email = "administrator@example.com",
                        DisplayName = "Admin"
                    },
                    "Secur3_adm1n_P@ssw0rd",
                    new string[] { "admin" }
                ),
                (
                    new ApplicationUser(){
                        UserName = "testuser@example.com",
                        Email = "testuser@example.com",
                        DisplayName = "Test User"
                    },
                    "1234567",
                    new string[] { }
                )
            };

            foreach (var testAccount in testAccounts)
            {
                if (await userManager.FindByNameAsync(testAccount.User.UserName) is null)
                {
                    await userManager.CreateAsync(testAccount.User, testAccount.Password);
                    foreach (var role in testAccount.Roles)
                    {
                        await userManager.AddToRoleAsync(testAccount.User, role);
                    }
                }
            }
        }

        private static async Task InitStaff(ApplicationDbContext dbContext)
        {
            if (dbContext.Staff.Any() || dbContext.Positions.Any())
            {
                return;
            }

            Position position1 = new()
            {
                Name = "Должность 1"
            };
            Position position2 = new()
            {
                Name = "Должность 2"
            };

            await dbContext.Positions.AddRangeAsync(new Position[] {
                position1,
                position2
            });

            await dbContext.Staff.AddRangeAsync(new Staff[] {
                new Staff() {
                    Name = "Test",
                    Surname = "Test",
                    Patronymic = "Test",
                    Position = position1,
                    BirthYear = 2000,
                    AssignmentDate = DateTime.Now
                },
                new Staff() {
                    Name = "A",
                    Surname = "B",
                    Patronymic = "C",
                    Position = position2,
                    BirthYear = 1980,
                    AssignmentDate = DateTime.Now
                },
            });

        }
    }
}
