﻿using Microsoft.AspNetCore.StaticFiles;
using System.ComponentModel.DataAnnotations;

namespace Web_053502_Sachivko.Utils.ValidationAttributes
{
    public class YearOfBirthAttribute : ValidationAttribute
    {
        private static readonly int _maxAge = 150;

        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value is null)
            {
                return ValidationResult.Success;
            }

            if (value is not int birthYear)
            {
                return new ValidationResult("Not an integer");
            }

            int currentYear = DateTime.Today.Year;
            if (birthYear > currentYear)
            {
                return new ValidationResult("Year of birth cannot be greater than current year");
            }

            int minYear = currentYear - _maxAge;
            if (birthYear < minYear)
            {
                return new ValidationResult($"Year of birth cannot be less than {minYear}");
            }
            return ValidationResult.Success;
        }
    }
}
